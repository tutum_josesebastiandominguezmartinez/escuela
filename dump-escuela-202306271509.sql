--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3 (Homebrew)
-- Dumped by pg_dump version 15.3 (Homebrew)

-- Started on 2023-06-27 15:09:33 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE escuela;
--
-- TOC entry 3628 (class 1262 OID 16389)
-- Name: escuela; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE escuela WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'C';


\connect escuela

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3629 (class 0 OID 0)
-- Name: escuela; Type: DATABASE PROPERTIES; Schema: -; Owner: -
--

ALTER DATABASE escuela SET "TimeZone" TO 'America/Mexico_City';


\connect escuela

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA public;


--
-- TOC entry 3630 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 216 (class 1259 OID 17386)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 214 (class 1259 OID 17369)
-- Name: t_alumnos; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.t_alumnos (
    id_t_usuarios integer NOT NULL,
    activo integer NOT NULL,
    ap_materno character varying(255),
    ap_paterno character varying(255),
    nombre character varying(255)
);


--
-- TOC entry 217 (class 1259 OID 17449)
-- Name: t_calificaciones; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.t_calificaciones (
    id_t_calificaciones bigint NOT NULL,
    calificacion real NOT NULL,
    fecha_registro timestamp with time zone,
    id_t_usuarios integer,
    id_t_materias integer
);


--
-- TOC entry 215 (class 1259 OID 17381)
-- Name: t_materias; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.t_materias (
    id_t_materias integer NOT NULL,
    activo integer NOT NULL,
    nombre character varying(255)
);


--
-- TOC entry 3619 (class 0 OID 17369)
-- Dependencies: 214
-- Data for Name: t_alumnos; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.t_alumnos VALUES (1, 1, 'Down', 'Dow', 'John');


--
-- TOC entry 3622 (class 0 OID 17449)
-- Dependencies: 217
-- Data for Name: t_calificaciones; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3620 (class 0 OID 17381)
-- Dependencies: 215
-- Data for Name: t_materias; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.t_materias VALUES (1, 1, 'Matemáticas');
INSERT INTO public.t_materias VALUES (2, 1, 'Programación');
INSERT INTO public.t_materias VALUES (3, 1, 'Ingeniería de Software');


--
-- TOC entry 3631 (class 0 OID 0)
-- Dependencies: 216
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.hibernate_sequence', 80, true);


--
-- TOC entry 3470 (class 2606 OID 17375)
-- Name: t_alumnos t_alumnos_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.t_alumnos
    ADD CONSTRAINT t_alumnos_pkey PRIMARY KEY (id_t_usuarios);


--
-- TOC entry 3474 (class 2606 OID 17453)
-- Name: t_calificaciones t_calificaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.t_calificaciones
    ADD CONSTRAINT t_calificaciones_pkey PRIMARY KEY (id_t_calificaciones);


--
-- TOC entry 3472 (class 2606 OID 17385)
-- Name: t_materias t_materias_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.t_materias
    ADD CONSTRAINT t_materias_pkey PRIMARY KEY (id_t_materias);


--
-- TOC entry 3475 (class 2606 OID 17454)
-- Name: t_calificaciones fkbungr5clyxhgwhqtf5kyk5nud; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.t_calificaciones
    ADD CONSTRAINT fkbungr5clyxhgwhqtf5kyk5nud FOREIGN KEY (id_t_usuarios) REFERENCES public.t_alumnos(id_t_usuarios);


--
-- TOC entry 3476 (class 2606 OID 17459)
-- Name: t_calificaciones fkc3m09buxx75xv9s5cud2d865v; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.t_calificaciones
    ADD CONSTRAINT fkc3m09buxx75xv9s5cud2d865v FOREIGN KEY (id_t_materias) REFERENCES public.t_materias(id_t_materias);


-- Completed on 2023-06-27 15:09:33 CST

--
-- PostgreSQL database dump complete
--

